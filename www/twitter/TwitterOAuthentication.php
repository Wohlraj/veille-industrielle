<?php

          namespace Twitter;

          use Twitter\TwitterOAuth as OAuth;
          use Twitter\TwitterException;

          /**
           * Classe que monta o cabe�alho de autoriza��o
           * oAuth para ser usado na integra��o com o twitter
           * @author Andrey Knupp Vital
           * @copyright (C)2011-2012
           * @package Twitter
           */
          final class TwitterOAuthentication {

                 /**
                  * M�todo de requisi��o que ser� usado na assinatura
                  * Deve ser o mesmo m�todo que ser� usado na requisi��o
                  * @var string
                  */
                 private $signatureMethod;

                 /**
                  * URL em que ser� feita a assinatura
                  * Deve ser a mesma URL para qual a requisi��o ser� enviada 
                  * @var string
                  */
                 private $signatureURL;

                 /**
                  * Par�metros de requisi��o com oAuth
                  * @var Object 
                  */
                 private $oAuthParams;

                 /**
                  * Objeto com os dados de autentica��o
                  * @var Object
                  */
                 private $authentication;

                 /**
                  * Constr�i o objeto recebendo como par�metro um objeto respons�vel
                  * por manter os dados necess�rios para uma autentica��o com o twitter
                  * @param \Twitter\TwitterAuthentication $authentication 
                  */
                 public function __construct( \Twitter\TwitterAuthentication $authentication ) {
                        $this->oAuthParams = new \ArrayObject ( );
                        $this->authentication = $authentication;
                        $this->attachOAuthParams();
                 }

                 /**
                  * Recupera o objeto que representa os dados de autentica��o para uso interno
                  * @throws \Twitter\TwitterException se n�o houver uma inst�ncia de TwitterAuthentication
                  * @return \Twitter\TwitterAuthentication Object()
                  */
                 final private function getAuthentication() {
                        if ( $this->authentication instanceOf \Twitter\TwitterAuthentication ) {
                               return $this->authentication;
                        } else throw new \Twitter\TwitterException( 'N�o foi poss�vel receber a autentica��o' );
                 }

                 /**
                  * Esse m�todo � respons�vel por adicionar os 
                  * par�metros necess�rios para uma autentica��o com o twitter
                  * no array de par�metros oAuth ( ArrayObject $oAuthParams )
                  */
                 final private function attachOAuthParams ( ) {
                        $Authentication = $this->getAuthentication();
                        $this->oAuthParams->offsetSet ( OAuth::CONSUMER_KEY , $Authentication->getConsumerKey() ) ;
                        $this->oAuthParams->offsetSet ( OAuth::OAUTH_NONCE , $this->buildNOnce() ) ;
                        $this->oAuthParams->offsetSet ( OAuth::SIGNATURE_METHOD , 'HMAC-SHA1' ) ;
                        $this->oAuthParams->offsetSet ( OAuth::OAUTH_TIMESTAMP , time ( ) ) ;
                        $this->oAuthParams->offsetSet ( OAuth::OAUTH_TOKEN , $Authentication->getAccessToken() ) ;
                        $this->oAuthParams->offsetSet ( OAuth::OAUTH_VERSION , '1.0' ) ;
                 }
                 
                 /**
                  * Cria a string de requisi��o ou ( base ) para 
                  * a assinatura com oAuth ser emitida com sucesso
                  * @param array $parameters
                  * @throws RuntimeException se n�o houver os par�metros oAuth em seu array
                  * @return string
                  */
                 final private function buildRequestString ( array $parameters = array ( ) ) {
                        if ( $this->oAuthParams->count() ) {
                               $oAuthParams = $this->oAuthParams->getArrayCopy() ;
                               $signatureParams = array_merge ( $oAuthParams , $parameters ) ;
                               ksort ( $signatureParams ) ;
                               $method = $this->getSignatureMethod();
                               $parse = parse_url  ( $this->getSignatureURL() ) ;
                               $url = sprintf ( '%s://%s%s' , $parse [ 'scheme' ] , $parse [ 'host' ] , $parse [ 'path' ] );
                               forEach ( $signatureParams as $param => $value ) $p [ ] = sprintf ( "%s=%s" , $param , $value ) ;
                               $base = sprintf ( '%s&%s&%s' , $method , rawurlencode ( $url ) , rawurlencode ( implode ( '&' , $p ) ) ) ;
                               return $this->buildSignature( $base ) ;
                        } else throw new \RuntimeException ( 'N�o � poss�vel criar a base para assinatura quando n�o temos os par�metros oAuth' ) ; 
                 }
                 
                 /**
                  * Cria a assinatura hmac-sha1 para a base recebida
                  * a implementa��o do hmac-sha1 foi feita seguindo a
                  * documenta��o da RFC 2104 p�gina 2 e 3, 4
                  * @see http://www.ietf.org/rfc/rfc2104.txt 
                  * @param string $base
                  * @return string
                  */
                 final private function buildSignature ( $base ) {
                        $compositeKey = $this->getAuthentication()->getConsumerSecret() ;
                        $tokenSecret = $this->getAuthentication()->getAccessTokenSecret() ;
                        $key = sprintf ( '%s&%s' , $compositeKey , $tokenSecret ) ;
                        if ( strlen ( $key ) > 0x40 ) 
                             $k = pack ( 'H*' , sha1 ( $key ) ) ;
                        $k = str_pad ( $k , 64 , chr ( 0x00 ) ) ;
                        $p = str_repeat ( chr ( 0x36 ) , 0x40 ) ;
                        $o = str_repeat ( chr ( 0x5C ) , 0x40 ) ;
                        $s = pack ( 'H*' , sha1 ( sprintf ( '%s%s' ,( $k ^ $p ) , $base ) ) ) ;
                        $hmac = pack ( 'H*' , sha1 ( sprintf ( '%s%s' , ( $k ^ $o ) , $s ) ) ) ;
                        return base64_encode ( $hmac ) ;
                 }
                 
                 /** 
                  * Cria a string de autoriza��o para poder ser emitida 
                  * no cabe�alho da requisi��o que ser� enviada para o twitter
                  * @param array $parameters
                  * @return string
                  */
                 final public function buildAuthorization ( array $parameters = array () ) {
                        if ( $this->oAuthParams->count() ) {
                               $signature = $this->buildRequestString( $parameters ) ;
                               $this->oAuthParams->offsetSet( OAuth::SIGNATURE , $signature ) ;
                               $this->oAuthParams->ksort();
                               for ( $Iterator = $this->oAuthParams->getIterator() ; $Iterator->valid() ; $Iterator->next() ) 
                                      $Authorization [ ] = sprintf ( '%s="%s"' , $Iterator->key() , rawurlencode ( $Iterator->current() ) ) ;
                               return sprintf ( 'Authorization: OAuth %s' , implode ( ', ' , $Authorization ) ) ; 
                        } else throw new \RuntimeException ( 'N�o � poss�vel criar a autoriza��o com oAuth se n�o tivermos os par�metros necess�rios' ) ;
                 }
                 
                 /**
                  * Gera um 'oauth_nonce' para a autoriza��o oauth 
                  * Essa fun��o retorna um hash md5 com uma base de 
                  * random pseudo para ser enviado na requisi��o
                  * @param integer $length
                  * @return string
                  */
                 final private function buildNOnce ( $length = 12 ) {
                       srand ( ( float ) microtime ( ) * 0x989680 ) ;
                       for ( $i = 0 , $l = ( int ) $length ; $i < $l ; ++ $i ) 
                              $k [ ] = chr ( rand ( 0x20 , 0x7E ) ) ;
                       return md5 ( substr ( microtime ( ) . implode ( $k ) , 0 , $length ) ) ;
                 }
                 /**
                  * Seta a url em que a assinatura ser� validada
                  * @param string $url 
                  */
                 public function setSignatureURL ( $url ) {
                        $this->signatureURL = $url ;
                 }
                 
                 /**
                  * Seta o m�todo de requisi��o que ser� enviado
                  * @param string $signatureMethod 
                  */
                 public function setSignatureMethod( $signatureMethod ) {
                        $this->signatureMethod = strtoupper ( $signatureMethod ) ;
                 }
                 
                 /**
                  * Recupera o m�todo de requisi��o que ser� utilizado 
                  * na assinatura de autentica��o do usu�rio
                  * @return string
                  */
                 public function getSignatureMethod() {
                         return $this->signatureMethod;
                 }

                 /**
                  * Recupera a URL na qual a autentica��o ser� emitida
                  * a mesma dever� ser enviada para a mesma URL setada
                  * @return string
                  */
                 public function getSignatureURL ( ) {
                         return $this->signatureURL ;
                 }

          }

          