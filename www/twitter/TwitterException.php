<?php

          namespace Twitter ;
          
          /**
           * Dispara uma exce��o, classe reservada para exce��es 
           * disparadas na integra��o com o Twitter 
           * @author Andrey Knupp Vital
           * @copyright (C)2011-2012
           * @package Twitter
           */
          
          class TwitterException extends \Exception {
                    
                    /**
                     * Constr�i uma exce��o personalizada e reservada apenas para 
                     * exce��es que ser�o disparadas nas classes de integra��o com o Twitter
                     * @param string  $message
                     * @param integer $code
                     */
                    public function __construct ( $message , $code = 0 ) {
                           return parent::__construct ( $message , $code ) ;
                    }
                    
          }
          

