<?php

          namespace Twitter;

          /**
           * Entidade que representa os dados de autentica��o
           * de uma aplica��o no twitter , os dados s�o armazenados
           * para uso na autentica��o e utiliza��o das api's que necessitam
           * de uma autentica��o oAuth para o retorno dos dados
           * 
           * @link https://dev.twitter.com/start
           * @link https://dev.twitter.com/docs/api-faq
           * @link https://dev.twitter.com/docs/auth
           * 
           * @author Andrey Knupp Vital
           * @copyright (C)2011-2012
           * @package Twitter
           */
          class TwitterAuthentication {
                 
                 /**
                  * Chave de consumidor
                  * @var string
                  */
                 
                 private $consumerKey;
                 
                 /**
                  * Senha do consumidor 
                  * @var string
                  */
                 private $consumerSecret;
                 
                 /**
                  * Token de acesso
                  * @var string
                  */
                 private $accessToken;
                 
                 /**
                  * Token de acesso
                  * @var string
                  */
                 private $accessTokenSecret;

                 /**
                  * Recupera a chave do consumidor
                  * @return String
                  */
                 public function getConsumerKey() {
                        return $this->consumerKey;
                 }
                 /**
                  * Seta a chave do consumidor 
                  * @param string $consumerKey 
                  */
                 public function setConsumerKey( $consumerKey ) {
                        $this->consumerKey = $consumerKey;
                 }
                 /**
                  * Recupera a senha do consumidor
                  * @return string
                  */
                 public function getConsumerSecret() {
                        return $this->consumerSecret;
                 }

                 /**
                  * Seta a senha do consumidor
                  * @param string $consumerSecret 
                  */
                 public function setConsumerSecret( $consumerSecret ) {
                        $this->consumerSecret = $consumerSecret;
                 }

                 /**
                  * Recupera o token de acesso
                  * @return string
                  */
                 public function getAccessToken() {
                        return $this->accessToken;
                 }
                 
                 /**
                  * Seta o token de acesso da aplica��o
                  * @param string $accessToken 
                  */
                 public function setAccessToken( $accessToken ) {
                        $this->accessToken = $accessToken;
                 }

                 /**
                  * Recupera o token de acesso
                  * @return string
                  */
                 public function getAccessTokenSecret() {
                        return $this->accessTokenSecret;
                 }

                 /**
                  * Seta o token de acesso 
                  * @param string $accessTokenSecret 
                  */
                 public function setAccessTokenSecret( $accessTokenSecret ) {
                        $this->accessTokenSecret = $accessTokenSecret;
                 }

          }