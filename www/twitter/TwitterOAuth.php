<?php

          namespace Twitter;

          /**
           * Constantes que representam os par�metros que ser�o
           * atrelados ao cabe�alho de autentica��o com o Twitter
           * @author Andrey Knupp Vital
           * @copyright (C)2011-2012
           * @package Twitter
           */
          interface TwitterOAuth {                 
                 const CONSUMER_KEY = 'consumer_key';
                 const OAUTH_NONCE = 'oauth_nonce';
                 const SIGNATURE_METHOD = 'oauth_signature_method';
                 const OAUTH_TOKEN = 'oauth_token';
                 const OAUTH_TIMESTAMP = 'oauth_timestamp';
                 const OAUTH_VERSION = 'oauth_version';
                 const SIGNATURE = 'oauth_signature';

          }