<?php
/**
Fonctions appel�es en page d'accueil pour la veille
*/
function get_last_tweets() {
	$text = '';
	$nbr_tweets = number_of_tweets();
	$cpt = 0;
	
	if(is_tagged())
		$result = mysql_query('SELECT nom1 FROM navimo WHERE tag=1 ORDER BY rand()');
	else
		$result = mysql_query('SELECT nom1 FROM navimo ORDER BY rand()');
		
	while($row = mysql_fetch_assoc($result) AND (is_tagged() OR $cpt<$nbr_tweets)) {
		$tweet = find_tweet_about($row['nom1']);
		if($tweet != ''){
			$text .= $tweet;
			$cpt++;
		}
	}
	return $text;
}

function get_last_feed() {
	$text = '';
	$nbr_feed = number_of_feed();
	$cpt = 0;
	
	if(is_tagged())
		$result = mysql_query('SELECT feed,nom1 FROM navimo WHERE feed!="" AND feed!="neant" AND tag=1 ORDER BY rand()') or die(mysql_error());
	else
		$result = mysql_query('SELECT feed,nom1 FROM navimo WHERE feed!="" AND feed!="neant" ORDER BY rand()') or die(mysql_error());
		
	while($row = mysql_fetch_assoc($result) AND (is_tagged() OR $cpt<$nbr_feed)) {
		$rss = read_rss($row['feed']);
		// print_r($rss);
		if(isset($rss['date'])) {
			$date = rssdate($rss['date']);
			$age = $date - rssdate(time());
			// echo $age;
			if($age<nbr_days()){
				if($age==0)
					$age = "aujourd'hui";
				else
					$age = 'il y a '.$age.' jours';
				$text .= '<u>'.$row['nom1'].'</u> '.$age.' - '.html_entity_decode($rss['text']).'<br>';
				$cpt++;
			}
		}
	}
	return $text;
}

function get_last_tweets_from_base() {
	$text = '';
	
	if(is_tagged())
		$result = mysql_query("SELECT nom1,feed FROM navimo WHERE tag=1 AND feed LIKE '%twitter%' ORDER BY rand()");
	else
		$result = mysql_query("SELECT nom1,feed FROM navimo WHERE feed LIKE '%twitter%' ORDER BY rand() LIMIT 0,".number_of_tweets()."");
		
	while($row = mysql_fetch_assoc($result)) {
		$feed = $row['feed'];
		$id = '';
		$username = '';
		
		if(preg_match("/user_timeline/",$feed)) {
			preg_match("/\d+/",$feed,$id);
			$id = $id['0'];
		}
		else
			$username = preg_replace('\'https://twitter.com/\'', '', $feed);
		
		$text .= tweet_from($id,$username);
		// echo "retrieving tweet from ".$username."<br>";
	}
	return $text;
}
?>