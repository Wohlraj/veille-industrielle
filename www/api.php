<?php
//Google reader subscriptions
function get_feed($name) {
	$url = "https://ajax.googleapis.com/ajax/services/feed/find?v=1.0&q=".urlencode($name);//."&userip=".$_SERVER['REMOTE_ADDR'];
	// echo $url;
	
	$json = file_get_contents($url);
	$results = json_decode($json, true);
	
	// echo "<pre>";
	// print_r($results['responseData']);
	// echo "</pre>";
	
	if(isset($results['error']))
		api_error($results['error']['message']);
	if(isset($results['responseData']['entries']['0']['url']) && $results['responseData']['entries']['0']['url']!='')
		return $results['responseData']['entries']['0']['url'];
	else
		return 'neant';
}

function read_rss($feed) {
	$url = "https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=".urlencode($feed);//."&userip=".$_SERVER['REMOTE_ADDR'];
	// echo $url;
	
	$json = file_get_contents($url);
	$results = json_decode($json, true);
	
	// echo "<pre>";
	// print_r($results);
	// echo "</pre>";
	
	if(!isset($results['responseData']['feed']['entries']['0'])){
		mysql_query('UPDATE navimo SET feed="neant" WHERE feed="'.$feed.'"');
		return 'Flux non conforme';
	}
	$link = $results['responseData']['feed']['entries']['0']['link'];
	$contentSnippet = htmlentities($results['responseData']['feed']['entries']['0']['contentSnippet']);
	$title = htmlentities($results['responseData']['feed']['entries']['0']['title']);
	$text = "<a href=\"".$link."\" target=\"_blank\">".$title."</a><p>".$contentSnippet."</p>";
	// return $results['responseData']['feed']['entries']['0']['contentSnippet'];
	// return $results['responseData']['feed']['entries']['0']['content'];
	// return $results['responseData']['feed']['entries']['0']['publishedDate'];
	// return $results;
	return array('text' => $text, 'date' => $results['responseData']['feed']['entries']['0']['publishedDate']);
}

//possibilit� de restreindre les r�sultats � certains types : https://developers.google.com/places/documentation/supported_types
function find_place($name) {
	$key = 'AIzaSyD5mItEvFOxsEP9emstaOopzgSV2kD9S-s';
	$name = urlencode($name);
	$url = 'https://maps.googleapis.com/maps/api/place/textsearch/json?sensor=false&key='.$key.'&query='.$name;
	// echo $url;
	
	$json = file_get_contents($url);

	$results = json_decode($json, true);
	
	api_error($results['status']);
	return $results['results']['0']['formatted_address'];
}

//ATTENTION : 100 requ�tes par jour max.
function find_website($name) {
	$key = 'AIzaSyD5mItEvFOxsEP9emstaOopzgSV2kD9S-s';
	$cx = '010477866225547099920:mvp2lwmownu';
	$search = urlencode($name);
	$url = "https://www.googleapis.com/customsearch/v1?key=".$key."&cx=".$cx."&q=".$search."&callback=json&num=1";
	// echo $url;
	
	$json = file_get_contents($url);
	// $json = file_get_contents('api_callback.txt');

	$start = 'json(';
	$end = ');';

	$substr_start_pos = strpos($json, $start) + strlen($start);
	$substr_length = strlen($json) - $substr_start_pos - (strlen($json) - strrpos($json, $end));

	$json = substr($json, $substr_start_pos, $substr_length);

	$results = json_decode($json, true);	 
	
	// echo "<pre>";
	// print_r($results);
	// echo "</pre>";
	if(isset($results['error']))
		api_error($results['error']['message']);
	return $results['items']['0']['link'];
}

function find_tweet_about($name) {
	spl_autoload_register(function($class){
	   require_once sprintf('%s.php' , $class);
	});	
	
	$Authentication = new \Twitter\TwitterAuthentication();
	
	$Authentication->setConsumerKey('jsYhK13K4xOvNFZ8re0Bg');
	$Authentication->setConsumerSecret('mOnroQklt2Sdd8KV1gyksTffvCRYj0HxjWURxLN9A');
	$Authentication->setAccessToken('636883128-Q0asEeIIieKBg9KlgYqJ8ULJOLeHGvsoQzFRaWSe');
	$Authentication->setAccessTokenSecret('hJ2EXneLMWqUiiBwaSpFKqLDzs9tPQCHwnAFPt8h8M');
	
	$OAuth = new Twitter\TwitterOAuthentication($Authentication);
	
	// $URL = 'https://api.twitter.com/1/users/search.json';
	$URL = 'http://search.twitter.com/search.json';
	$name = '\"'.$name.'\"';
	$Data = Array('q' => $name, 'lang' => 'en', 'rpp' => '1');

	$OAuth->setSignatureURL($URL . '?' . http_build_query($Data , false , '&')) ;
	$OAuth->setSignatureMethod('GET'); 

	$curl = curl_init();
	curl_setopt( $curl , CURLOPT_URL ,  $URL . '?' . http_build_query($Data , false , '&')) ;
	curl_setopt( $curl , CURLOPT_HEADER , false);
	curl_setopt( $curl , CURLOPT_HTTPHEADER , Array($OAuth->buildAuthorization( $Data)));
	curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true);
	curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );

	$response = json_decode(curl_exec($curl)) ;

	$str = '';
	if(isset($response->results['0'])){
		//recherche de mot-cl�s
		$interesting = get_keywords();
		$res = false;
		foreach($interesting as $word){
			$res = preg_match("/".$word."/i",($response->results['0']->text));
			if($res)
				break;
		}
		if($res){
			$str = format_tweet(
				$response->results['0']->text,
				$response->results['0']->from_user,
				$response->results['0']->id_str,
				$response->results['0']->created_at,
				$word);
		}
	}
	return $str;
}

function tweet_from($id='',$username='') {
	spl_autoload_register(function($class){
	   require_once sprintf('%s.php' , $class);
	});	
	
	$Authentication = new \Twitter\TwitterAuthentication();
	
	$Authentication->setConsumerKey('jsYhK13K4xOvNFZ8re0Bg');
	$Authentication->setConsumerSecret('mOnroQklt2Sdd8KV1gyksTffvCRYj0HxjWURxLN9A');
	$Authentication->setAccessToken('636883128-Q0asEeIIieKBg9KlgYqJ8ULJOLeHGvsoQzFRaWSe');
	$Authentication->setAccessTokenSecret('hJ2EXneLMWqUiiBwaSpFKqLDzs9tPQCHwnAFPt8h8M');
	
	$OAuth = new Twitter\TwitterOAuthentication($Authentication);
	
	$URL = 'https://api.twitter.com/1/users/show.json';
	if($id!='')
		$Data = Array('user_id' => $id, 'include_entities' => 'true');
	else
		$Data = Array('screen_name' => $username, 'include_entities' => 'true');		
	
	// print_r($Data);

	$OAuth->setSignatureURL($URL . '?' . http_build_query($Data , false , '&')) ;
	$OAuth->setSignatureMethod('GET'); 

	$curl = curl_init();
	curl_setopt( $curl , CURLOPT_URL ,  $URL . '?' . http_build_query($Data , false , '&')) ;
	curl_setopt( $curl , CURLOPT_HEADER , false);
	curl_setopt( $curl , CURLOPT_HTTPHEADER , Array($OAuth->buildAuthorization( $Data)));
	curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true);
	curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );

	$response = json_decode(curl_exec($curl)) ;
	// echo "<pre>";
	// print_r($response);
	// echo "</pre>";
	
	if(!isset($response->error) && isset($response->status)) {
		return $str = format_tweet(
					$response->status->text,
					$response->screen_name,
					$response->status->id_str,
					$response->status->created_at,
					"",$id);
	}
	else {
		echo "<pre>";
		print_r($response->error);
		echo "</pre>";
	}
}

function get_twitter_status() {
	spl_autoload_register(function($class){
	   require_once sprintf('%s.php' , $class);
	});	
	
	$Authentication = new \Twitter\TwitterAuthentication();
	
	$Authentication->setConsumerKey('jsYhK13K4xOvNFZ8re0Bg');
	$Authentication->setConsumerSecret('mOnroQklt2Sdd8KV1gyksTffvCRYj0HxjWURxLN9A');
	$Authentication->setAccessToken('636883128-Q0asEeIIieKBg9KlgYqJ8ULJOLeHGvsoQzFRaWSe');
	$Authentication->setAccessTokenSecret('hJ2EXneLMWqUiiBwaSpFKqLDzs9tPQCHwnAFPt8h8M');
	
	$OAuth = new Twitter\TwitterOAuthentication($Authentication);
	
	$URL = 'https://api.twitter.com/1/account/rate_limit_status.json';
	$Data = Array();

	$OAuth->setSignatureURL($URL . '?' . http_build_query($Data , false , '&')) ;
	$OAuth->setSignatureMethod('GET'); 

	$curl = curl_init();
	curl_setopt( $curl , CURLOPT_URL ,  $URL . '?' . http_build_query($Data , false , '&')) ;
	curl_setopt( $curl , CURLOPT_HEADER , false);
	curl_setopt( $curl , CURLOPT_HTTPHEADER , Array($OAuth->buildAuthorization( $Data)));
	curl_setopt( $curl , CURLOPT_RETURNTRANSFER , true);
	curl_setopt( $curl , CURLOPT_SSL_VERIFYPEER , false );

	$response = json_decode(curl_exec($curl)) ;
	return array($response->remaining_hits,$response->reset_time);
}
?>