<?php
/**
Fonctions qui font principalement de nombreux appels aux fonctions de api.php
Et autres fonctions "outils"
*/

function count_data() {
	$result1 = mysql_query('SELECT COUNT(*) FROM navimo') or die(mysql_error);
	// echo mysql_result($result1,0).' entr�es dans la base.<br><br>';
	return mysql_result($result1,0);
}

function number_of_tweets() {
	$result = mysql_query('SELECT tweet FROM param WHERE id=1') or die(mysql_error);
	$row = mysql_fetch_assoc($result);
	return $row['tweet'];
}
function number_of_feed() {
	$result = mysql_query('SELECT feed FROM param WHERE id=1') or die(mysql_error);
	$row = mysql_fetch_assoc($result);
	return $row['feed'];
}

function rssdate($date) {
	return date("d/m/Y",strtotime($date));
}

function format_tweet($text,$author,$id,$time,$word="",$id2="") {
	$text = preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.%-]*(\?\S+)?)?)?)@', '<a href="$1" target="_blank">$1</a>', $text);

	$str = "<blockquote class=\"twitter-tweet\" lang=\"fr\">";
	$str .= "<p>".$text."</p>&mdash;";
	$str .= $author." (@".$author.") <a href=\"https://twitter.com/".$author."/status/".$id."\">".$time."</a>";
	$str .= "</blockquote>";
	// $str .= $word;
	$str .= '<p style="font-size:8px" align="right">'.$id2.'</p>';	
	return $str;
}

function update_feed($nbr) {
	$result = mysql_query('SELECT id,nom1 FROM navimo WHERE feed="" AND feed NOT LIKE '%twitter%' ORDER BY rand( ) LIMIT 0,'.$nbr.'');
	?>
	<table border="1">
	<?php
	while ($row = mysql_fetch_assoc($result)) {
		$feed = get_feed($row['nom1']);
		mysql_query('UPDATE navimo SET feed="'.$feed.'" WHERE id="'.$row['id'].'"');
		$str = '<tr>';
		// $str .= '<td>'.$row['id'].'</td>';
		// $str .= '<td>'.$row['nom1'].'</td>';
		$str .= '<td>'.$feed.'</td></tr>';
		echo $str;
	}
	?>
	</table>
	<?php	
	$result = mysql_query('SELECT COUNT(*) FROM navimo WHERE feed=""');
	echo 'Encore '.mysql_result($result,0).' entr�es sans flux';
}

function update_websites($nbr) {
	$result = mysql_query('SELECT id,nom1,website FROM navimo WHERE website="" ORDER BY rand( ) LIMIT 0,'.$nbr.'');
	?>
	<table border="1">
	<?php
	while ($row = mysql_fetch_assoc($result)) {
		$website = find_website($row['nom1']);
		mysql_query('UPDATE navimo SET website="'.$website.'" WHERE id="'.$row['id'].'"');
		$str = '<tr><td>'.$row['id'].'</td>';
		$str .= '<td>'.$row['nom1'].'</td>';
		$str .= '<td>'.$website.'</td></tr>';
		echo $str;
	}
	?>
	</table>
	<?php
	$result = mysql_query('SELECT COUNT(*) FROM navimo WHERE website=""');
	echo 'Encore '.mysql_result($result,0).' entr�es sans site web';
}

function update_places($nbr) {
	$result = mysql_query('SELECT id,nom1,pays FROM navimo WHERE ville="" ORDER BY rand( ) LIMIT 0,'.$nbr.'');
	?>
	<table border="1">
	<?php
	$hashtable_reversed = get_countries_hashtable(true);
	while ($row = mysql_fetch_assoc($result)) {
		$country = $hashtable_reversed[$row['pays']];
		$place = find_place($row['nom1'].' '.$country);
		sleep(1.5);
		$place = explode(",",$place);
		$rue = $place['0'];
		$ville = $place['1'];
		mysql_query('UPDATE navimo SET rue="'.$rue.'",ville="'.$ville.'" WHERE id="'.$row['id'].'"');
		$str = '<tr><td>'.$row['id'].'</td>';
		$str .= '<td>'.$row['nom1'].'</td>';
		$str .= '<td>'.$rue.'</td>';
		$str .= '<td>'.$ville.'</td>';
		$str .= '<td>'.$row['pays'].'/'.$country.'</td></tr>';
		echo $str;
	}
	?>
	</table>
	<?php
}

function merge_duplicates() {
	$before = count_data();
	
	mysql_query('CREATE TABLE navimo2 LIKE navimo');
	mysql_query('
		INSERT INTO navimo2
		SELECT 
		id,
		nom1,
		MAX(nom2) AS nom2,
		MAX(rue) AS rue, 
		MAX(cp) AS cp, 
		MAX(ville) AS ville,
		MAX(pays) AS pays,
		MAX(tel) as tel,
		MAX(email) AS email,
		MAX(respo) AS respo,
		MAX(website) AS website,
		MAX(feed) AS feed,
		MAX(tag) AS tag
		FROM navimo GROUP BY nom1');
	mysql_query('DROP TABLE navimo');
	mysql_query('RENAME TABLE navimo2 TO navimo');
		
	$after = count_data();
	$nbr_deleted = $before-$after;
	echo 'Fusion des doublons effectu�e. '.$nbr_deleted.' entr�es fusionn�es.';	
}

function get_countries_hashtable($reverse = false){
	$handle2 = fopen("code_pays_EN.txt", "r");
	$handle = fopen("code_pays_FR.txt", "r");
	//on parcourt d'abord le fichier francais afin d'avoir en majorit� des noms de pays en anglais (francais �cras�)
	
	$hashtable = array();
	
	if ($handle) {
		while (($buffer = fgets($handle, 4096)) !== false) {
			$buffer = chop($buffer); //removes \n
			$word = explode(";",$buffer);
			if($reverse)
				$hashtable[$word['1']] = $word['0'];
			else
				$hashtable[$word['0']] = $word['1'];
		}
		fclose($handle);
	}
	if ($handle2) {
		while (($buffer = fgets($handle2, 4096)) !== false) {
			$buffer = chop($buffer); //removes \n
			$word = explode(";",$buffer);
			if($reverse)
				$hashtable[$word['1']] = $word['0'];
			else
				$hashtable[$word['0']] = $word['1'];
		}
		fclose($handle2);
	}
	
	return $hashtable;
}

//troncature des noms de pays trop longs (espace � la fin)
function repair_countries(){
	$cpt = 0;
	$result = mysql_query('SELECT id,pays FROM navimo');
	while ($row = mysql_fetch_assoc($result)) {
		if(strlen($row['pays'])==3){
			$country = $row['pays']['0'].$row['pays']['1'];
			mysql_query('UPDATE navimo SET pays="'.$country.'" WHERE id="'.$row['id'].'"');
		}
		if($row['pays']=='UK')
			mysql_query('UPDATE navimo SET pays="GB" WHERE id="'.$row['id'].'"');
		$cpt++;
	}
	echo 'Format de pays invalides : '.$cpt.' changements.<br>';
}

function repair_names() {
//CASSE DES NOMS------------------------------------------------------
	$cpt = 0;
	$result = mysql_query('SELECT id,nom1,rue FROM navimo');
	while ($row = mysql_fetch_assoc($result)) {
		$name = ucwords(strtolower($row['nom1']));
		$rue = ucwords(strtolower($row['rue']));
		mysql_query("UPDATE navimo SET nom1='".$name."',rue='".$rue."' WHERE id='".$row['id']."'");
		$cpt++;
	}
	echo '<b>Casse de '.$cpt.' noms.<br></b>';
	
//MOTS CLES INTERDITS-------------------------------------------------
	$cpt=0;
	$query = "SELECT id,nom1 FROM navimo";
	// echo $query;	
	$result = mysql_query($query);
	while ($row = mysql_fetch_assoc($result)) {
		$name_init = $row['nom1'];
		$str = '['.$name_init.']-->[';
		$name = $name_init;
		$invalid_keywords = invalid_keywords();
		foreach($invalid_keywords as $word){
			$name = str_replace($word, "", $name);
		}
		$name = str_replace(",", " ", $name);
		$name = str_replace("&", "And", $name);
		$str .= $name.']<br>';
		// echo $str;
		if(strcmp($name,$name_init)!=0){
			mysql_query("UPDATE navimo SET nom1='".$name."' WHERE id='".$row['id']."'");
			$cpt++;
			echo $str;
		}
	}
	echo '<b>Mot-cl�s de '.$cpt.' noms.<br></b>';
	
//CARACTERES EN FIN DE NOM-------------------------------------------------
	$cpt=0;
	$result = mysql_query("SELECT id,nom1 FROM navimo WHERE nom1 LIKE '% ' OR nom1 LIKE '%.' OR nom1 LIKE '%,'");
	while ($row = mysql_fetch_assoc($result)) {
		$name = $row['nom1'];
		echo '['.$name.']-->[';
		$name = substr_replace($name,'',-1,1);
		echo $name.']<br>';
		mysql_query("UPDATE navimo SET nom1='".$name."' WHERE id='".$row['id']."'");
		$cpt++;
	}
	echo '<b>Caract�res surnum�raires de '.$cpt.' noms.<br></b>';
	
}

function repair_websites(){
	$invalid_websites = invalid_websites();
	$query = "SELECT id,website FROM navimo WHERE ";
	foreach($invalid_websites as $word){
		$query .= "website LIKE '%".$word."%' OR ";
		// echo $query;
	}
	$query .= "website LIKE '%.pdf'";
	
	$result = mysql_query($query);
	// echo $query;
	while ($row = mysql_fetch_assoc($result)) {
		mysql_query('UPDATE navimo SET website="neant" WHERE id="'.$row['id'].'"');
		echo 'id '.$row['id'].' : website "'.$row['website'].'" bloqu�.<br>';
	}
	
	$result = mysql_query("SELECT id,website FROM navimo WHERE 
		website!='' AND 
		website!='neant' AND 
		website NOT LIKE 'www%' AND 
		website NOT LIKE 'http%'
	");
	while ($row = mysql_fetch_assoc($result)) {
		mysql_query('UPDATE navimo SET website="neant" WHERE id="'.$row['id'].'"');
		echo 'id '.$row['id'].' : website "'.$row['website'].'" supprim�.<br>';
	}
}

function repair_feed() {
	$invalid_websites = invalid_websites();
	$query = "SELECT id,feed FROM navimo WHERE ";
	foreach($invalid_websites as $word){
		$query .= "feed LIKE '%".$word."%' OR ";
	}
	$query .= "feed LIKE '%.pdf'";
	
	$result = mysql_query($query);
	while ($row = mysql_fetch_assoc($result)) {
		mysql_query('UPDATE navimo SET feed="neant" WHERE id="'.$row['id'].'"');
		echo 'id '.$row['id'].' : flux "'.$row['feed'].'" bloqu�.<br>';
	}
}

function code_countries() {	
	$hashtable = get_countries_hashtable();
		
	// print_r($hashtable);
	$cpt = 0;
	$result = mysql_query('SELECT id,pays FROM navimo ORDER BY pays');
	while ($row = mysql_fetch_assoc($result)) {
		if(array_key_exists($row['pays'], $hashtable)){
			// echo $row['pays'].'->'.$hashtable[$row['pays']].'<br>';
			$cpt++;
			mysql_query("UPDATE navimo SET pays='".$hashtable[$row['pays']]."' WHERE id='".$row['id']."'") 
				or die("Erreur sur la mise � jour des codes pays : ".mysql_error());
		}
	}
	echo 'Codes pays : '.$cpt.' changements.<br>';
}

function api_error($error){
	if($error=="OVER_QUERY_LIMIT" || $error=="Daily Limit Exceeded")
		die('Google a retourn� <b>'.$error.'</b>');
}

function search_name($name,$field="nom"){
	if($field=="nom")
		$result = mysql_query("SELECT * FROM navimo WHERE nom1 LIKE '%".$name."%' OR nom2 LIKE '%".$name."%'") or die(mysql_error());
	else	
		$result = mysql_query("SELECT * FROM navimo WHERE ".$field." LIKE '%".$name."%'") or die(mysql_error());
		
	if( mysql_num_rows($result)==0) {
		echo 'Pas de r�sultat.';
	}
	else
	{
		echo mysql_num_rows($result).' r�sultats.';
	?>
		<form method="post" action="index.php?p=delete">
		<table border="0" align="center">
	<?php
		while ($row = mysql_fetch_assoc($result)) {
			// $str = '<tr><td>'.$row['id'].'</td>';
			$str = '<tr><td>'.$row['nom1'].'</td>';			
			$str .= '<td><img src="images/flags/'.$row['pays'].'.png" alt="'.$row['pays'].'"></td>';
			$str .= '<td>'.$row['rue'].'</td>';
			$str .= '<td>'.$row['cp'].'</td>';
			$str .= '<td>'.$row['ville'].'</td>';
			$str .= '<td>'.$row['tel'].'</td>';
			$str .= '<td>'.$row['respo'].'</td>';
			$str .= '<td>';
			if($row['email']!='')
				$str .= '<a href="mailto:'.$row['email'].'" href="_blank"><img src="images/Android-Email.png" width="25px"></a>';
			$str .= '</td>';
			$str .= '<td>';
			if($row['website']!='' && $row['website']!='neant'){
				if(preg_match('/http:\/\//',$row['website']))
					$str .= '<a href="'.$row['website'].'" href="_blank">';
				else
					$str .= '<a href="http://'.$row['website'].'" href="_blank">';
				$str .= '<img src="images/Android-Browser.png" width="25px"></a>';
			}
			$str .= '</td>';
			$str .= '<td>';
			if($row['feed']!='' && $row['feed']!='neant'){
				if(preg_match('/twitter/',$row['feed'])>0)
					$img = 'images/Android-Twitter.png';
				else
					$img = 'images/Android-RSS.png';				
				$str .= '<a href="'.$row['feed'].'" href="_blank"><img src="'.$img.'" width="25px"></a>';
			}
			$str .= '</td>';
			$str .= '<td><input type="checkbox" value="'.$row['id'].'" name="delete[]" /></td>';
			$str .= '<td><a href="index.php?p=edit&id='.$row['id'].'"><img src="images/chrome_toolsmenu.gif" width="25px"></a></td></tr>';
			$str .= '<input type="hidden" value="'.$name.'" name="search">';
			$str .= '<input type="hidden" value="'.$field.'" name="field">';
			echo $str;
		}
	?>
		<tr><td colspan="13" align="right"><input type="submit" value="Supprimer" /></td></tr>
		</table>
	<?php 
	}
	?>
	</form>
	<?php
}

function edit($id) {
	echo 'Info : si l\'entr�e ne poss�de pas de site internet, ou de flux RSS, ins�rer "n�ant"<br><br>';
	$result = mysql_query("SELECT * FROM navimo WHERE id='".$id."'") or die(mysql_error());
	?>
	<form method="post" action="index.php?p=edit&id=<?php echo $id; ?>">
	<table border="0" align="center">
	<?php
	$row = mysql_fetch_assoc($result);
	$str = '<tr><td>Nom</td><td><input type="text" value="'.$row['nom1'].'" name="nom1" /></td></tr>';
	$str .= '<tr><td>Rue</td><td><input type="text" value="'.$row['rue'].'" name="rue" /></td></tr>';
	$str .= '<tr><td>CP</td><td><input type="text" value="'.$row['cp'].'" name="cp" size="5" /></td></tr>';
	$str .= '<tr><td>Ville</td><td><input type="text" value="'.$row['ville'].'" name="ville" /></td></tr>';	
	$str .= '<tr><td>Pays</td><td><input type="text" value="'.$row['pays'].'" name="pays" size="1" /></td></tr>';
	$str .= '<tr><td>T�l.</td><td><input type="text" value="'.$row['tel'].'" name="tel" /></td></tr>';
	$str .= '<tr><td>Email</td><td><input type="text" value="'.$row['email'].'" name="email" /></td></tr>';
	$str .= '<tr><td>Respo</td><td><input type="text" value="'.$row['respo'].'"  name="respo" /></td></tr>';
	$str .= '<tr><td>Site web</td><td><input type="text" value="'.$row['website'].'" name="website" /></td></tr>';
	$str .= '<tr><td>Flux</td><td><input type="text" value="'.$row['feed'].'" name="feed" /></td></tr>';
	$str .= '<tr><td>Tag</td><td><input type="checkbox"';
		if($row['tag'])
			$str .= 'checked="checked"';
	$str .= 'name="tag" /></td></tr>';
	$str .= '<tr><td colspan="2" align="center"><input type="submit" value="Valider" /></td></tr>';
	echo $str;
	?>
	</table>
	</form>
	<?php
}

function add_entry() {
	echo 'Info : si l\'entr�e ne poss�de pas de site internet, ou de flux RSS, ins�rer "n�ant"<br><br>';
	?>
	<form method="post" action="index.php?p=add">
	<table border="0" align="center">
	<?php
	$str = '<tr><td>Nom</td><td><input type="text" name="nom1" /></td></tr>';
	$str .= '<tr><td>Rue</td><td><input type="text" name="rue" /></td></tr>';
	$str .= '<tr><td>CP</td><td><input type="text" name="cp" size="5" /></td></tr>';
	$str .= '<tr><td>Ville</td><td><input type="text" name="ville" /></td></tr>';	
	$str .= '<tr><td>Pays</td><td><input type="text" name="pays" size="1" /></td></tr>';
	$str .= '<tr><td>T�l.</td><td><input type="text" name="tel" /></td></tr>';
	$str .= '<tr><td>Email</td><td><input type="text" name="email" /></td></tr>';
	$str .= '<tr><td>Respo</td><td><input type="text" name="respo" /></td></tr>';
	$str .= '<tr><td>Site web</td><td><input type="text" name="website" /></td></tr>';
	$str .= '<tr><td>Flux/Twitter</td><td><input type="text" name="feed" /></td></tr>';
	$str .= '<tr><td>Tag</td><td><input type="checkbox" name="tag" /></td></tr>';
	$str .= '<tr><td colspan="2" align="center"><input type="submit" value="Valider" /></td></tr>';
	echo $str;
	?>
	</table>
	</form>
	<?php
}
?>